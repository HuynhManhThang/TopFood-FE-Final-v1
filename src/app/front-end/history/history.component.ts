import { Component, OnInit } from '@angular/core';
import { Cart } from 'src/app/entity/Cart';
import { HistoryService } from '../../service/history/history.service';
import { History } from 'src/app/entity/History';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  history : History = new History();
  historys: History[];
  userId: number;
  currentUser = '';
  trangThai: String;
  totalPrice = 0;
  carts: Cart[];
  page = 1;
  pageSize = 10;
  constructor(
    private historyService : HistoryService
  ) { }

  ngOnInit(): void {
    this.currentUser = localStorage.getItem("currentUser");
    if (this.currentUser) {
      this.userId = JSON.parse(localStorage.getItem("currentUser")).id;
      this.getHistorys(this.userId, "");
    }
  }

  getHistorys(user: number, order: String) {
    if(order == null){
      this.getHistorys(this.userId, "");
    }
    this.historyService.getHistory(user, order).subscribe(data => {
      if (data) {
        data.forEach(x => {
          if(x.status == 1) {
            x.status = "Đã thanh toán";
          }
          if (x.status == 0) {
            x.status = "Đang xử lý";
          }
          if (x.status == "2") {
            x.status = "Thanh toán không thành công";
          }
          if (x.status == "3") {
            x.status = "Đơn hàng đã hủy";
          }
        });
        this.historys = data; 
      }
      else{
        this.historys = [];
      }
    });
  }
}