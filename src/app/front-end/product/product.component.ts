import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem } from 'src/app/entity/CartItem';
import { Category } from 'src/app/entity/Category';
import { Product } from 'src/app/entity/Product';
import { CartService } from 'src/app/service/cart/cart.service';
import { CategoryService } from 'src/app/service/category/category.service';
import { ProductService } from 'src/app/service/product/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  products: Product[];
  top4Category: Category[]; 
  product: Product = new Product();
  cartItem : CartItem[];
  currentUser = '';
  cart: CartItem = new CartItem();
  displaySuccess = "none";
  displayError = "none";
  userId: number;
  count: number;
  productDetail: Product;
  isShowModal: boolean = true;
  page = 1;
  pageSize = 16;
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private cartService: CartService,
    private router: Router
  ) {
    this.currentUser = localStorage.getItem("currentUser");
    if (this.currentUser) {
      this.userId = JSON.parse(this.currentUser).id;
    }
   }

  ngOnInit(): void {
    // this.getProducts();
    this.getTop4Category();
    this.searchProduct("");
  }

  getProducts() {
    this.productService.getAll().subscribe(data => {
      if (data) {
        data.forEach(x => {
          if (x.priceSale == 0) {
            x.priceSale = "";
          }
        });
        this.products = data;
      }
      
    })
  }
  
  getId(id) {
    this.productService.getProduct(id).subscribe(data => {
      this.productDetail = data;
    })
  }
  getTop4Category() {
    this.categoryService.getTop4Category().subscribe(data => {
      this.top4Category = data;
    })
  }

  OnAddCart(product: Product) {
    this.currentUser = localStorage.getItem("currentUser");
    if (this.currentUser) {
      this.cart.userId = JSON.parse(localStorage.getItem("currentUser")).id;
      this.cart.price = product.price;
      this.cart.productId = product.id;
      this.cart.productName = product.productName;
      this.cart.image = product.image;
      this.cartService.create(this.cart).subscribe(() => {
        this.getCountCart();
        this.openModal();
      }, err => {
        if(err.error) {
          this.openModalEror();
        }
      })
    } else {
      this.router.navigateByUrl('/login');
    }
  }
  searchProduct(productName: any) {
    if (productName == null) {
      this.getProducts();
    }
    this.productService.search(productName).subscribe(result => {
      if (result) {
        if (result) {
          result.forEach(x => {
            if (x.priceSale == 0) {
              x.priceSale = "";
            }
          });
          this.products = result;
        }
        console.log(this.products);
      }else {
        this.products = [];
      }
      
    });
  }

  getCountCart() {
    if (this.userId) {
      this.cartService.getCountCart(this.userId).subscribe((data) => {
        if (data) {
          this.count = data;
        }
      });
    } else {
      this.count = 0;
    }
  }

  onCloseHandled() {
    this.displaySuccess = "none";
  }

  openModal() {
    this.displaySuccess = "block";
  }

  onCloseError() {
    this.displayError = "none";
  }

  openModalEror() {
    this.displayError = "block";
  }

}