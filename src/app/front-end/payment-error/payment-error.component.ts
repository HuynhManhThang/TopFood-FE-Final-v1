import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OderService } from 'src/app/service/order/oder.service';

@Component({
  selector: 'app-payment-error',
  templateUrl: './payment-error.component.html',
  styleUrls: ['./payment-error.component.scss']
})
export class PaymentErrorComponent implements OnInit {
  userId: number;
  currentUser = '';
  constructor(
    private activeRouter: ActivatedRoute,
    private orderService: OderService,
  ) { 
    
  }

  ngOnInit(): void {
    this.currentUser = localStorage.getItem("currentUser");
    if (this.currentUser) {
      this.userId = JSON.parse(localStorage.getItem("currentUser")).id;
    }
    this.activeRouter.queryParams.subscribe(param => {
      if (param['token']) {
        this.orderService.fail(this.userId, param['token']).subscribe(() => {
          console.log("Payment failed");
        })
      }
    });
  }

}
