import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/entity/User';
import { TokenStorageService } from 'src/app/service/login/token-storage.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.scss']
})
export class UserManagerComponent implements OnInit {
  users : User[];
  user :  User;
  userId : number;
  name : String;
  password : String;
  search: User = new User();
  page = 1;
  pageSize = 10;
  display = "none";
  constructor(
    private tokenStorage: TokenStorageService,
    private userService : UserService
    ) { }

  ngOnInit(): void {
    // this.getAll();
    this.searchUser("");
  }
  getAll(){
    this.userService.getListUser().subscribe( data=> {   
      this.users = data;
    })
  }
  delete(id: number) {
    this.userId = id;
    this.name = this.tokenStorage.getUser().username;
}

checkDelete(){
  this.userService.removeUser(this.userId, this.name)
    .subscribe(
      data => {
        console.log(data);
        this.getAll();
      },
      error => {
        console.log(error);
        this.openModal();
      }
      )
}

searchUser(userName: String) {
  if(userName == null){
    this.getAll();
  }
  this.userService.search(userName).subscribe(result => {
    if (result) {
      this.users = result;
      console.log(this.users);
    }else {
      this.users = [];
    }
    
  });
}

openModal() {
  this.display = "block";
}

onCloseHandled() {
  this.display = "none";
}
}
