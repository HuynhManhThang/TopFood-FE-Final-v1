import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/entity/Category';
import { ExcelResponse } from 'src/app/entity/ExcelResponse';
import { Product } from 'src/app/entity/Product';
import { ProductRespon } from 'src/app/entity/ProductRespon';
import { Supplier } from 'src/app/entity/Supplier';
import { CategoryService } from 'src/app/service/category/category.service';
import { ProductService } from 'src/app/service/product/product.service';
import { SupplierService } from 'src/app/service/supplier/supplier.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {
  productResponse:ProductRespon = new ProductRespon();
  product: Product = new Product();
  categorys: Category[];
  suppliers: Supplier[];
  form: FormGroup;
  IsModelShow : boolean = false;
  file : File;
  expireDate : Date;
  excel : ExcelResponse = new ExcelResponse();
  formf = new FormGroup({
    file: new FormControl('', [Validators.required])
  });
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private supplierService: SupplierService,
    private router: Router,
    private fb: FormBuilder,

  ) {

    this.form = this.fb.group({
      productName: ['', [
        Validators.required,
      ]],
      price: ['', [
        Validators.required,
      ]],
      priceSale: ['', [
        Validators.required,
      ]],
      categoryId: ['', [
        Validators.required,
      ]],
      supplierId: ['', [
        Validators.required,
      ]],
      description: ['', [
        Validators.required,
      ]],
      quantity: ['', [
        Validators.required,
      ]],
    });
   }
    
  ngOnInit(): void {
    this.getCategorys();
    this.getSuppliers();
  }

  addProduct() {
    this.product.expired = this.expireDate;
    this.productService.addProduct(this.product).subscribe(data =>{
      this.productResponse = data;
      console.log(data);
      window.alert("Thêm thành công");
          this.router.navigateByUrl('/admin/manager')
    });
  }

  getCategorys() {
    this.categoryService.getListCategory().subscribe(data =>{
      if(data) {
        this.categorys = data;
        console.log(this.categorys)
      }
    })
  }
  getSuppliers() {
    this.supplierService.getAll().subscribe(data =>{
      if(data) {
        this.suppliers = data;
        console.log(this.suppliers)
      }
    })
  }

  public onFileChanged(event) {
    //Select File
    this.product.file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(this.product.file);
    reader.onload = () => {
      this.product.imageBase64 = reader.result.toString();
    };
  }

  get f() { return this.formf.controls; }

  get productName() {
    return this.form.get('productName');
  }

  get price() {
    return this.form.get('price');
  }

  get expired() {
    return this.form.get('expired');
  }

  get priceSale() {
    return this.form.get('priceSale');
  }

  get description() {
    return this.form.get('description')
  }

  get categoryId() {
    return this.form.get('categoryId')
  }

  get supplierId() {
    return this.form.get('supplierId')
  }

  get quantity() {
    return this.form.get('quantity')
  }

  checkValidateProductName() {
    this.product.productName = this.product.productName.trim();
  }

  submitForm(){
    this.productService.addProductByExcel(this.file).subscribe(data =>{    
     this.excel = data;
     this.IsModelShow = true;
     this.file = null;
    })
  }
  changeFile(event){
    this.file = event.target.files[0];
  }
  exit(){
    this.IsModelShow = false;
  }
  onChangeDate(event){
    this.expireDate =  event.target.value;
    
  }

 }